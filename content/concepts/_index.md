+++
date = "2017-05-12"
weight = 100

title = "Concept Designs"

aliases = [
    "/old-wiki/ConceptDesigns"
]
+++

The concept designs are a set of technical documents covering many aspects of the project, written through the history of the project, taking into account the status of the project at that time.
These documents cover topics that have been researched but not necessarily implemented in Apertis at the time of writing.

Information in these documents may be outdated, but nonetheless provide important context for the decision making process and overall vision of the project.

