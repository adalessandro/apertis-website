+++
date = "2019-12-03"
weight = 100

title = "Image building"

aliases = [
    "/old-wiki/Image_building"
]
+++

The process of getting from source code to an image suitable for loading
into a target device with Apertis is a clearly defined multi-step
process. The initial step is to build the source and package the
resulting artefacts into `.deb` packages. For the vast majority of
packages this is already done and provided by Apertis in the [package
repositories](https://repositories.apertis.org/). This page will not
cover this step of the process an will assume all required packages have
been successfully built and are available in the package repositories.

There are two further steps to generating an image:

  - Building an Operating System package (OSpack)
  - Combining the OSpack with a Hardware package (HWpack) to create an
    image

Both the process of building OSpacks and images from them is carried out
using [Debos](https://github.com/go-debos/debos). It is strongly
recommended to utilise the [appropriate Docker image builder
container](https://gitlab.apertis.org/infrastructure/apertis-image-recipes#building-in-docke)
to perform these steps, though the Apertis SDK image [running in a
VM]( {{< ref "/guides/virtualbox.md" >}} )
can be used with reduced reliability.

# Building OSpacks

The OSpack is a root file system containing all the generic software for
a particular image type and hardware architecture (e.g. the arm
architecture). Apertis provides stock Debos recipes in the form of
`.yaml` files in the
[apertis-image-recipes](https://gitlab.apertis.org/infrastructure/apertis-image-recipes)
gitlab repository, with a name of the form `apertis-ospack-*.yaml`, each
one providing a different balance of packages for different tasks.

It is best to build these packages images from the appropriate
image-builder docker container. This is documented in the
apertis-image-recipes
[README.md](https://gitlab.apertis.org/infrastructure/apertis-image-recipes#building-in-docker).

# Building an image with a HWpack

Unlike the OSpack, that is generic, the HWpack contains the bits needed
to boot on a specific target. The HWpack configuration typically
extracts an OSpack, creates an image containing the required partition
layout for the specific hardware, writes the extracted OSpack into it,
adds extra components necessary for the target and may perform some
tweaks to the image (tweaking configuration files etc). Finally the
image is compressed to minimise storage space.

Existing Apertis examples can be found in the [Apertis Image Recipes
repository](https://gitlab.apertis.org/infrastructure/apertis-image-recipes)
called `apertis-image-*.yaml`. The Debos documentation also provides a
[simple example](https://github.com/go-debos/debos#simple-example).
