+++
date = "2020-09-02"
weight = 100

title = "Release Schedule"

aliases = [
    "/old-wiki/Release_schedule",
    "/old-wiki/ReleaseNotes"
]
+++

Apertis has a three month release cycle. Development happens
through most of the cycle with emphasis on bug fixing towards the end.
The quarterly release cycle was chosen as it fits well with the fast
paced development of Apertis.

## Release timeline

The Apertis release cycle starts at the end of the previous release
cycle. At this point, new features are proposed, discussed and
implemented.

The **soft feature freeze** takes place about 4 weeks before the bug
fixing stops. Any features which you want to see in Apertis for the
release need to be implemented in the source code before this date. From
this date, only features which are already in Apertis can be polished
for the release. This was previously known as "soft freeze".

The **hard feature freeze** takes place about 2 weeks before the bug
fixing stops. At this point, only bug fixes can be made to Apertis code.
This was previously known as "hard freeze".

The **hard code freeze** is usually a 3-4 days before the stable
release. All bugs must be fixed before the bug fixing freeze. All
development stops and the final image is prepared for the release. This
was previously known as "bug fixing freeze".

The final image is published on the release day.

If you need to request a break (exception) for any of the above freezes,
you can do so by emailing the Apertis maintainers mailing list
([maintainers@lists.apertis.org](https://lists.apertis.org/listinfo/maintainers))
and getting the support of two or more Apertis maintainers.

## Releases

The latest releases are:

  - v2022dev0: [release notes]( {{< ref "/release/v2022dev0/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2022dev0/release_schedule.md" >}} )
  - v2021pre: [release notes]( {{< ref "/release/v2021pre/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2021pre/release_schedule.md" >}} )
  - v2020.4: [release notes]( {{< ref "/release/v2020.4/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2020.4/release_schedule.md" >}} )
  - v2019.6: [release notes]( {{< ref "/release/v2019.6/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2019.6/release_schedule.md" >}} )

The next releases will be:

  - v2022dev1: [release schedule]( {{< ref "/release/v2022dev0/release_schedule.md" >}} )
  - v2021.0: [release schedule]( {{< ref "/release/v2021.0/release_schedule.md" >}} )
  - v2020.5: [release schedule]( {{< ref "/release/v2020.5/release_schedule.md" >}} )
  - v2019.7: [release schedule]( {{< ref "/release/v2019.7/release_schedule.md" >}} )

For previous releases see:

  - v2020.3: [release notes]( {{< ref "/release/v2020.3/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2020.3/release_schedule.md" >}} )
  - v2019.5: [release notes]( {{< ref "/release/v2019.5/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2019.5/release_schedule.md" >}} )
  - v2021dev3: [release notes]( {{< ref "/release/v2021dev3/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2021dev3/release_schedule.md" >}} )
  - v2020.2: [release notes]( {{< ref "/release/v2020.2/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2020.2/release_schedule.md" >}} )
  - v2019.4: [release notes]( {{< ref "/release/v2019.4/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2019.4/release_schedule.md" >}} )
  - v2021dev2: [release notes]( {{< ref "/release/v2021dev2/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2021dev2/release_schedule.md" >}} )
  - v2020.1: [release notes]( {{< ref "/release/v2020.1/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2020.1/release_schedule.md" >}} )
  - v2019.3: [release notes]( {{< ref "/release/v2019.3/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2019.3/release_schedule.md" >}} )
  - v2021dev1: [release notes]( {{< ref "/release/v2021dev1/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2021dev1/release_schedule.md" >}} )
  - v2020.0: [release notes]( {{< ref "/release/v2020.0/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2020.0/release_schedule.md" >}} )
  - v2019.2: [release notes]( {{< ref "/release/v2019.2/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2019.2/release_schedule.md" >}} )
  - v2020pre: [release notes]( {{< ref "/release/v2020pre/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2020pre/release_schedule.md" >}} )
  - v2019.1: [release notes]( {{< ref "/release/v2019.1/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2019.1/release_schedule.md" >}} )
  - v2020dev0: [release notes]( {{< ref "/release/v2020dev0/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2020dev0/release_schedule.md" >}} )
  - v2019.0: [release notes]( {{< ref "/release/v2019.0/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2019.0/release_schedule.md" >}} )
  - v2019pre: [release notes]( {{< ref "/release/v2019pre/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2019pre/release_schedule.md" >}} )
  - v2019dev0: [release notes]( {{< ref "/release/v2019dev0/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/v2019dev0/release_schedule.md" >}} )
  - 18.12: [release notes]( {{< ref "/release/18.12/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/18.12/release_schedule.md" >}} )
  - 18.09: [release notes]( {{< ref "/release/18.09/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/18.09/release_schedule.md" >}} )
  - 18.06: [release notes]( {{< ref "/release/18.06/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/18.06/release_schedule.md" >}} )
  - 18.03: [release notes]( {{< ref "/release/18.03/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/18.03/release_schedule.md" >}} )
  - 17.12.1: [release notes]( {{< ref "/release/17.12.1/releasenotes.md" >}} )
  - 17.12: [release notes]( {{< ref "/release/17.12/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/17.12/release_schedule.md" >}} )
  - 17.09: [release notes]( {{< ref "/release/17.09/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/17.09/release_schedule.md" >}} )
  - 17.06: [release notes]( {{< ref "/release/17.06/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/17.06/release_schedule.md" >}} )
  - 17.03: [release notes]( {{< ref "/release/17.03/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/17.03/release_schedule.md" >}} )
  - 16.12: [release notes]( {{< ref "/release/16.12/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/16.12/release_schedule.md" >}} )
  - 16.09: [release notes]( {{< ref "/release/16.09/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/16.09/release_schedule.md" >}} )
  - 16.06: [release notes]( {{< ref "/release/16.06/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/16.06/release_schedule.md" >}} )
  - 16.03: [release notes]( {{< ref "/release/16.03/releasenotes.md" >}} ), [release schedule]( {{< ref "/release/16.03/release_schedule.md" >}} )
  - 15.12: [release notes]( {{< ref "/release/15.12/releasenotes.md" >}} )
  - 15.09: [release notes]( {{< ref "/release/15.09/releasenotes.md" >}} )
  - 15.06: [release notes]( {{< ref "/release/15.06/releasenotes.md" >}} )
  - 15.03: [release notes]( {{< ref "/release/15.03/releasenotes.md" >}} )
  - 14.12: [release notes]( {{< ref "/release/14.12/releasenotes.md" >}} )


## Security support

  - When a release happens it automatically supersedes the previous
    release, therefore previous releases are discontinued and do not
    receive any security support.
  - Superseded releases shall be removed from Apertis build
    infrastructure a short time after the current release.
