+++
date = "2018-10-02"
weight = 100

title = "folks-telepathy-retrieve-contacts"

aliases = [
    "/qa/test_cases/folks-telepathy.md",
    "/old-wiki/QA/Test_Cases/folks-telepathy",
    "/old-wiki/QA/Test_Cases/folks-telepathy-retrieve-contacts"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/
